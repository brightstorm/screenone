-- run the following data within phpmysqladmin. 
-- you need to first select database
use bs_screenone_dev;


-- change for your localhost setting
update txp_prefs set val = 'dev.screenoneprinters.co.uk' where name ='siteurl';
	
-- local site path
update txp_prefs set val = '/local/www/data/dev/screenone/public_html' where name ='path_to_site';
	
-- local files path	
update txp_prefs set val ='/local/www/data/dev/screenone/public_html/files' where name='file_base_path';	
	